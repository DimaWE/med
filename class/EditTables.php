<?php

class EditTables 
{
    public $schema = [];

    public function __construct(array $schema)
    {
        $this->schema = $schema;
    }

    public function view(): void 
    {
        $dbSchema = $this->schema;
        $t = $_GET['table'];

        if($t) {
            $tableSchema = $this->schema[$t]['columns'];
            uasort($tableSchema, [$this, 'sortColumns']);
            $tableData = $this->getDataTable($t);
        }

        include "views/index.php";
    }

    public function create() : void 
    {
        $db = DB::getDB();

        $t = $_POST['table'];
        $tableSchema = $this->schema[$t]['columns'];

        if($this->schema[$t]) {
            $sql = "INSERT INTO {$t} SET ";
            foreach($tableSchema as $column => $colData) {
                $sql .= "{$column} = '".$_POST[$column]."',"; 
            }
            $sql = substr($sql, 0, -1);
            $sql .= ";";
            $db->exec($sql);
        }
    }

    public function update(): void 
    {
        $db = DB::getDB();

        $t = $_POST['table'];
        $tableSchema = $this->schema[$t]['columns'];

        if($this->schema[$t]) {
            $sql = "UPDATE {$t} SET ";
            foreach($tableSchema as $column => $colData) {
                $sql .= "{$column} = '".$_POST[$column]."',"; 
            }
            $sql = substr($sql, 0, -1);
            $sql .= " WHERE id = :id;";
            echo $sql;
            $smtp = $db->prepare($sql);
            $smtp->execute(['id' => $_POST['id']]);
        }

    }

    public function delete(): void 
    {
        $db = DB::getDB();
        $t = $_GET['table'];
        if($this->schema[$t]) {
            $sql = "DELETE FROM {$t} WHERE id = :id";
            $smtp = $db->prepare($sql);
            $smtp->execute(['id' => $_GET['id']]);
        }
    }

    private function getDataTable(string $table): array 
    {
        $db = DB::getDB();
        $tableData = $this->schema[$table];

        if($tableData) {

            $smtp = $db->prepare("SELECT * FROM {$tableData['tableName']} ORDER BY id ASC");
            $smtp->execute(['table' => $table]);
            $result = $smtp->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } else {
            return [];
        }
    }

    public function sortColumns(array $a, array $b): int
    {
        if ($a['sort'] == $b['sort']) {
            return 0;
        }
        return ($a['sort'] < $b['sort']) ? -1 : 1;
    }
}