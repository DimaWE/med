<?php

class DB {

	private $pdo;

	private $host = 'localhost';
	private $user = 'root';
	private $password = 'secret';
	private $dbName = 'med';

    private static $instance;

	private function __construct() {
		$dsn = "mysql:host={$this->host};dbname={$this->dbName};charset=utf8";
		$this->pdo = new PDO($dsn, $this->user, $this->password); 
    }

	static function getDB(): PDO
	{
		if(!self::$instance) {
			self::$instance = new static();
		}

		return self::$instance->pdo;
	}


}