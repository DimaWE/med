<?php

class CreateDB
{
    private $struct = [];

    public function __construct(array $struct)
    {
        $this->struct = $struct;
    }

    public function create(): void 
    {
        foreach($this->struct as $table) {
            $this->createTable($table);
        }
    }

    public function setData($dbData): bool 
    {
        $db = DB::getDB();
        $sql = '';
        foreach ($dbData as $table => $data) {
            foreach($data as $dataCol) {
                $sql .= "INSERT INTO `{$table}` SET ";
                foreach($dataCol as $columnName => $columnData) {
                    $sql .= "{$columnName} = '{$columnData}',";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ";";
            }
        }

        try {
            $db->exec($sql);
        }
        catch(Errror $e) {
            return false;
        }
        return true;
    }

    private function createTable(array $table): bool 
    {
        $db = DB::getDB();

        $sql = "
            CREATE TABLE IF NOT EXISTS `{$table['tableName']}`
            ( `id` INT NOT NULL AUTO_INCREMENT ,
        ";
        
        foreach($table['columns'] as $columnName => $column) {
            $sql .= "`{$columnName}` {$column['type']} NOT NULL ";
            if($column['default'])
                $sql .= "DEFAULT '{$column['default']}'";
            $sql .= " ,";
        }

        $sql .= "PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        
        try {
            $db->exec($sql);
        }
        catch(Errror $e) {
            return false;
        }
        return true;
    }
}
