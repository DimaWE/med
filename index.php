<?php
include_once "class/DB.php";
include_once "class/EditTables.php";

[$dbSchema] = include_once "dbStruct.php";

$edit = new EditTables($dbSchema);
$edit->view();

?>
