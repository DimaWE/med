<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Таблицы</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
    <link rel="stylesheet" href="web/style.css">
</head>
<body>
<div class="container">
    <form method="GET">
        <div class="form-group">
            <label for="table">Выберите таблицу</label>
            <select class="form-control" name="table" id="table">
            <?php foreach($dbSchema as $table): ?>
                    <option 
                        value="<?=$table['tableName']?>"
                        <?=$t && $t == $table['tableName'] ? ' selected' : ''?>
                    >
                        <?=$table['tableTitle']?>
                    </option>
            <?php endforeach; ?>
            </select>
        </div> 
        <button type="submit" class="btn btn-primary">Загрузить</button>  
    </form>

    <?php if($t): ?>
        <div class="add">
            <button class="large material-icons" title="Добавить запись" id="newCol">add_circle_outline</button>
        </div>
        <table class="table">
        <thead>
            <tr>
                <?php foreach($tableSchema as $column => $colData): ?>
                    <th scope="col"><?=$colData['title']?></th>
                <?php endforeach; ?>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($tableData as $data): ?>
                <tr style="background-color: <?=$data['color']?>">
                    <?php foreach($tableSchema as $column => $colData): ?>
                        <td id="<?=$column?>_val_<?=$data['id']?>" data-id=<?=$data['id']?> data-col="<?=$column?>">
                            <?=$data[$column]?>
                        </td>
                    <?php endforeach; ?>  
                    <td>
                        <button class="editCol large material-icons" data-id="<?=$data['id']?>">create</button>
                        <button class="delCol large material-icons" data-id="<?=$data['id']?>"" data-table="<?=$t?>"">delete</button>
                    </td>
                </tr>                  
            <?php endforeach; ?>           
        </tbody>
        </table>

        <!-- Modal -->
        <div class="modal fade" id="editCol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="formAdd" name="formAdd" action="ajax.php" method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modal-title"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php foreach($tableSchema as $column => $colData): ?>
                                <div class="form-group">
                                    <label for="<?=$column?>_in"><?=$colData['title']?></label>
                                    <input required="required" name="<?=$column?>" type="<?=$column == 'color' ? 'color': 'text'?>" class="form-control" id="<?=$column?>_in">
                                </div>
                            <?php endforeach; ?>
                            
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="id_in" name="id">
                            <input type="hidden" name="table" value="<?=$t?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary" id="btnSend">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="web/app.js"></script>
</body>
</html>
