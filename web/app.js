$('#newCol').on('click', function() {
    $('#formAdd')[0].reset();
    $('#id_in').val('');
    $('#modal-title').html('Добавить запись');
    $('#editCol').modal('show');
});

$('.editCol').on('click', function() {
    id = $(this).data('id');
    $('#id_in').val(id);
    $('td[data-id="' + id +'"]').toArray().forEach(item => {
       col = $(item).data('col');
       $('#' + col + '_in').val($(item).html().trim()); 
    });
    $('#modal-title').html('Изменить запись');
    $('#editCol').modal('show');
});

$('#formAdd').on('submit', function() {
    $.ajax({
        type: "POST",
        url: '/ajax.php?action=new',
        data: $(this).serialize(),
        success: function() {
            location.reload();
        }
      });
    return false;
});

$('.delCol').on('click', function() {
    id = $(this).data('id');
    table = $(this).data('table');
    $.ajax({
        type: "GET",
        url: `/ajax.php?action=delete&table=${table}&id=${id}`,
        success: function() {
            location.reload();
        }
      });
});