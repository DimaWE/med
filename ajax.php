<?php
include_once "class/DB.php";
include_once "class/EditTables.php";

[$dbSchema] = include_once "dbStruct.php";

$edit = new EditTables($dbSchema);


$action = $_GET['action'];

if($action == 'new') {
    if($_POST['id'])
        $edit->update();
    else 
        $edit->create();
} elseif($action == 'delete') {
    $edit->delete();
}

?>
