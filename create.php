<?php

include_once "class/DB.php";
include_once "class/CreateDB.php";

[$dbSchema, $dbData] = include_once "dbStruct.php";


$create = new CreateDB($dbSchema);
$create->create();
$create->setData($dbData);


